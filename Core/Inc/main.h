/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "cayenne_lpp.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOA
#define VCP_TX_Pin GPIO_PIN_2
#define VCP_TX_GPIO_Port GPIOA
#define TEMP_Pin GPIO_PIN_3
#define TEMP_GPIO_Port GPIOA
#define HUM_SIGNAL_Pin GPIO_PIN_4
#define HUM_SIGNAL_GPIO_Port GPIOA
#define HUM_SIGNAL_EXTI_IRQn EXTI4_IRQn
#define RST_Pin GPIO_PIN_6
#define RST_GPIO_Port GPIOA
#define LED_Pin GPIO_PIN_7
#define LED_GPIO_Port GPIOA
#define VDD_HUM_Pin GPIO_PIN_0
#define VDD_HUM_GPIO_Port GPIOB
#define NSS_Pin GPIO_PIN_11
#define NSS_GPIO_Port GPIOA
#define VDD_TEMP_Pin GPIO_PIN_12
#define VDD_TEMP_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define VCP_RX_Pin GPIO_PIN_15
#define VCP_RX_GPIO_Port GPIOA
#define DIO1_Pin GPIO_PIN_6
#define DIO1_GPIO_Port GPIOB
#define DIO0_Pin GPIO_PIN_7
#define DIO0_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define CAYENNE_LPP_MAX_BUFFER_SIZE CAYENNE_LPP_TEMPERATURE+CAYENNE_LPP_ANALOG_OUTPUT

ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

SPI_HandleTypeDef hspi3;

TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart2;

unsigned int tickNB;
uint8_t temp;
uint8_t sensorValueReady;
uint16_t clock_tick;

TIM_HandleTypeDef htim6;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
